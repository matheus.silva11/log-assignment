package com.avenuecode.matheus.logassignment;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class Estado {

	private String sigla;
	private String nome;

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado(String sigla, String nome) {
		super();
		this.sigla = sigla;
		this.nome = nome;
	}

	public Estado() {
		super();
	}

}
