package com.avenuecode.matheus.logassignment;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class LogassignmentApplication {

	Logger logger=LoggerFactory.getLogger(LogassignmentApplication.class);
	
	@GetMapping("/estado/{uf}")
    public Estado getUserById(@PathVariable String uf) {
		List<Estado> estados = getEstados();
		Estado estado = estados.stream().
				filter(e->e.getSigla().equals(uf.toUpperCase())).findAny().orElse(null);
		if(estado!=null){
			logger.info("Estado found : {} ",estado.getSigla()+ " - " + estado.getNome());
			return estado;
		}else{
			try {
				throw new Exception();
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Estado Not Found with UF : {}",uf);
			}
			return new Estado();
		}
    }
	
	private List<Estado> getEstados() {
        return Stream.of(new Estado("PE", "Pernambuco"),
				new Estado("PB", "Paraiba"),
				new Estado("SP", "São Paulo"),
				new Estado("BH", "Belo Horizonte"))
				.collect(Collectors.toList());
    }

    public static void main(String[] args) {
        SpringApplication.run(LogassignmentApplication.class, args);
    }

}
